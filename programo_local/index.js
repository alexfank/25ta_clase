const csvtojson = require('csvtojson');
const mysql = require('mysql2');

// Database credentials

const hostname = 'localhost',
    username = 'root',
    password = 'root',
    database = 'acamicadb'

    let con = mysql.createConnection({
        host: hostname,
        user: username,
        password: password,
        database: database,
    });

    con.connect((err) => {
        if (err) return console.error(
            'error: ' + err.message);

    con.query('DROP TABLE students',
    (err, drop) => {

        var createStatement = 
        'CREATE TABLE students(Name char(50), ' + 
        'Email char(50), Age int, city char(30))'

        con.query(createStatement, (err, drop) => {
            if (err)
            console.log('ERROR: ', err);
            console.log('Carga finalizada')
        });

    });
    });

    const filename = "Macintosh HD⁩/⁨Users⁩/⁨alexfank⁩/⁨Documents⁩/⁨Acámica⁩/⁨25ta_clase⁩/⁨programo_local⁩/data.csv"

    csvtojson().fromFile(filename).then(source => {

        for (var i=0; i < source.length; i++) {
            var Name = source[i]['Name'],
                Email = source[i]['Email'],
                Age = source[i]['Age'],
                City = source[i]['City']

            var insertStatement = 
            `INSERT INTO students values(?, ?, ?, ?)`;
            var items = [Name, Email, Age, City];

            con.query(insertStatement, items,
                (err, results, fields) => {
                    if (err) {
                        console.log('No se pudo insertar el registro en el item ', i + 1);
                        return console.log(err);
                    }
                });
        }
        console.log('Todos los items fueron almacenados satisfactoriamente')
    });